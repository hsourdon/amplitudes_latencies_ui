function [hf, tt, sigma0] = plot_single_abr(dat, filename, which_dB, gain, system, fig1, Ltube, Reverse)

% 1- Extract data 

prm=struct;

%initialisation of ABR wave parameters
prmlist={'fb','nsig0','nwaves'};
prmvals={[0.001,3],2,5};

for k=1:length(prmlist)
    if(~isfield(prm,prmlist{k})) 
        prm.(prmlist{k})=prmvals{k};
    end 
end

hsgn = Reverse; %1 is normal, -1 is in the animal facility

cson = 340; 

%get sound level parameters
%noise levels
try Lmin_nois = dat.stimparams.Lmin_nois; 
    Lmax_nois = dat.stimparams.Lmax_nois;
    dL_nois   = dat.stimparams.delL_nois; 
catch 
    Lmin_nois = 0;
    Lmax_nois = 0;
    dL_nois = 0;
end
if(dL_nois==0) 
    Lnois=Lmin_nois;
elseif(dL_nois>0)
    Lnois=Lmin_nois:dL_nois:Lmax_nois;
    Lnois = [0 Lnois];
elseif(dL_nois<0)
    Lnois=Lmax_nois:dL_nois:Lmin_nois;
    Lnois = [0 Lnois];
end

Lmin = dat.stimparams.Lmin;
Lmax = dat.stimparams.Lmax;
dL = dat.stimparams.delL;

if dL==0
    L = Lmin.*ones(1,length(Lnois));
elseif(dL>0)
    L = Lmin:dL:Lmax;
    Lnois=Lmin_nois*ones(1,length(L));
elseif(dL<0)
    L=Lmax:dL:Lmin;
    Lnois=Lmin_nois*ones(1,length(L));
end

if strcmp(system,'CERIAH')
    L=1:size(dat.hrav,2);
elseif strcmp(system,'Otophylab')
    L=dat.stimparams.L;
end

    
fmin = dat.stimparams.fmin; 
fmax = dat.stimparams.fmax; 
if(isfield(dat.stimparams,'delf')) 
    df = dat.stimparams.delf;
else 
    df = (fmax-fmin); 
end
if(df==0)
    f = fmin;
elseif(df>0)
    f = fmin:df:fmax;
elseif(df<0)
    f = fmax:df:fmin;
end

%1) Filter wave(s) whithin specified band
fb   = prm.fb; 
t0   = 1e-3.*dat.stimparams.espace./2;
ttube= Ltube/cson;
tt   = dat.tt1-t0-ttube;
dt   = tt(2)-tt(1);
hrfac= 1e6./gain; %rescale abr waveform to µV units

try 
    h = hsgn*hrfac.*dat.hrav;
    if size(h,3) > 1
        try
            id = abs(str2double(filename(end-16:end-15)));
            nf = f == id;
            h = squeeze(h(:,:,nf));
        catch 
            id = str2double(filename(end-15)); %5kHz
            nf = f == id;
            h = squeeze(h(:,:,nf));
        end
    end
catch
    h = hsgn*hrfac.*dat.hravsing;
end

h    = h-mean(h);
hf   = bandpass_filt(h,dt,fb);

%2) estimate background noise

if strcmp(system,'CERIAH')
    htp0=h(tt>20,:);
elseif strcmp(system,'Otophylab')
    tt = tt*1e3;
    htp0=h(tt>20,:);
else
    tt = tt*1e3;
    htp0=h(tt<=0,:);
end
sigma0=std(htp0);
sigma0(isnan(sigma0)) = 0;

% Manual annotation 
n = which_dB; 
interval = find(tt>0 & tt<12);

if (n<=length(L) && n>0)
    hold(fig1, 'off')
    hf = hf(interval,n);
    tt = tt(interval);
    sigma0 = sigma0(n); 
    plot(fig1, tt, hf, 'k','linewidth', 2);
    xlabel(fig1, 'Time (ms)', 'fontsize', 14)
    ylabel(fig1, 'Voltage (uV)', 'fontsize', 14)
    text(fig1, tt(end),hf(end),[num2str(L(n)) 'dB'],'fontsize',14, 'color', 'r')
else
    cla(fig1,'reset')
    text(fig1,0.45,0.5,'No more data', 'Color','r', 'FontSize',14)
end


end
