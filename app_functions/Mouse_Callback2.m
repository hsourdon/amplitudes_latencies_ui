% Callback function for each point
function distnew = Mouse_Callback2(hObj,~,action)
    persistent curobj xdata ydata ind ind2
    pos = get(gca,'CurrentPoint');
    textdata = findobj(gca,'Type','Text');
    for i=1:length(textdata)
        A(i) = textdata(i).Position(2);
    end
    
    switch action
      case 'down'
          curobj = hObj;
          xdata = get(hObj,'xdata');
          ydata = get(hObj,'ydata');
          [~,ind] = min(sum((xdata-pos(1)).^2+(ydata-pos(3)).^2,1));
          
          [~,ind2] = find(A==ydata(end));

          set(gcf,...
              'WindowButtonMotionFcn',  {@Mouse_Callback2,'move'},...
              'WindowButtonUpFcn',      {@Mouse_Callback2,'up'});
      case 'move'
          % vertical move

          distnew = ydata(ind)-pos(3);
          ydata = ydata-distnew;
          set(curobj,'ydata',ydata)
 
          try 
              textdata(ind2).Position(2) = textdata(ind2).Position(2)-distnew; 
          catch
              disp('error while moving');
          end

      case 'up'
          set(gcf,...
              'WindowButtonMotionFcn',  '',...
              'WindowButtonUpFcn',      '');
    end
end